const express = require('express');
const mongoose = require('mongoose');
const userSchema = require('./models/user');
const recipeSchema = require('./models/recipe');
const recipe = require('./models/recipe');

const app = express();

//middleware
app.use(express.json());

// MongoDB connection
mongoose.connect('mongodb://poli:password@mongorecetas:27017/recetas?authSource=admin')
.then(() => console.log('conectado a Mongo'))
.catch((error) => console.error(error))

app.get('/', (req, res) => {
    res.send('Hola')
})

app.post('/new_user', (req,res) => {
    const user = new userSchema(req.body);
    user.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error))
})

app.post('/new_recipe', (req,res) => {
    const recipe = new recipeSchema(req.body);
    recipe.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error))
})

app.get('/recipesbyingredient', async (req, res) => {
    const list_ingredients = req.body.ingredients.map((ingredient) => ingredient.name.toLowerCase());

    recipeSchema.aggregate([
            { $set: { es_subset: { $setIsSubset: [ "$ingredients.name", list_ingredients ] }} },
            { $match: {es_subset: true}},
            { $project: {es_subset: 0}}
        ]).exec()
            .then(resultados => {
                res.json(resultados)
            })
            .catch(err => {
                console.error(err);
            })
})

app.put('/rate', (req,res) => {
    const { recipeId, userId , rating } = req.body
    
    recipeSchema.updateOne(
        { _id: recipeId },
        [
            { $set: { ratings: {$concatArrays: [{$ifNull: ['$ratings', []]}, [{ userId: userId, rating: rating}]]}} },
            {$set: {avgRating: {$trunc: [{$avg:['$ratings.rating']},0]}}}
        ]
    )
    .then((data) => res.json(data))
    .catch((error) => {
        console.log(error)
        res.send("error")
    })
})

app.get('/recetas', (req, res) => {
    recipeSchema.find()
        .then((recipeSchema) => res.json(recipeSchema))
        .catch((error) => res.send(error))
})

app.get('/recipes', (req, res) => {
    const id_user = new mongoose.Types.ObjectId(req.body['userId']);
    recipeSchema.find({userId: id_user})
        .then((resultados) => res.json(resultados))
        .catch((error) => res.send(error))
})

app.listen(3000, () => console.log("escuchando en puerto 3000..."));